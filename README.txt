CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Questionnaires module allows you to create evaluations in Drupal.
These evaluations are a series of questions.
The answers, scores and results are shown during and after these evaluations.

Administrators can:
 - Provide feedback.
 - Check the answers and the results.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/questionnaires

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/questionnaires


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/documentation/install/modules-themes/modules-8
   for further information.


CONFIGURATION
-------------

No configuration is needed.


MAINTAINERS
-----------

Current maintainers:
 * Elmis David Hernandez (Elmis Hernandez) -
   https://www.drupal.org/u/elmis-hernandez

This project has been sponsored by:
 * ITSS
   Founded in 2013, ITSS is a human-sized company, dedicated to the design and
   implementation of web interfaces. Visit: https://itss.paris/ for more
   information.

<?php

namespace Drupal\questionnaires;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Type de questionnaires entity.
 *
 * @see \Drupal\questionnaires\Entity\TypeQuestionnairesEntity.
 */
class TypeQuestionnairesEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\questionnaires\Entity\TypeQuestionnairesEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished type de questionnaires entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published type de questionnaires entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit type de questionnaires entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete type de questionnaires entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add type de questionnaires entities');
  }

}

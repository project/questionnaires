<?php

namespace Drupal\questionnaires;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Type de questionnaires entities.
 *
 * @ingroup questionnaires
 */
class TypeQuestionnairesEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Type de questionnaires ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\questionnaires\Entity\TypeQuestionnairesEntity $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.type_questionnaires_entity.edit_form',
      ['type_questionnaires_entity' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}

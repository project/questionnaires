<?php

namespace Drupal\questionnaires;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for type_questionnaires_entity.
 */
class TypeQuestionnairesEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}

<?php

namespace Drupal\questionnaires\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Type de questionnaires entities.
 *
 * @ingroup questionnaires
 */
interface TypeQuestionnairesEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Type de questionnaires name.
   *
   * @return string
   *   Name of the Type de questionnaires.
   */
  public function getName();

  /**
   * Sets the Type de questionnaires name.
   *
   * @param string $name
   *   The Type de questionnaires name.
   *
   * @return \Drupal\questionnaires\Entity\TypeQuestionnairesEntityInterface
   *   The called Type de questionnaires entity.
   */
  public function setName($name);

  /**
   * Gets the Type de questionnaires creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Type de questionnaires.
   */
  public function getCreatedTime();

  /**
   * Sets the Type de questionnaires creation timestamp.
   *
   * @param int $timestamp
   *   The Type de questionnaires creation timestamp.
   *
   * @return \Drupal\questionnaires\Entity\TypeQuestionnairesEntityInterface
   *   The called Type de questionnaires entity.
   */
  public function setCreatedTime($timestamp);

}

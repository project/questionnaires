<?php

namespace Drupal\questionnaires\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Type de questionnaires type entities.
 */
interface TypeQuestionnairesEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}

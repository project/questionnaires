<?php

namespace Drupal\questionnaires\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Type de questionnaires type entity.
 *
 * @ConfigEntityType(
 *   id = "type_questionnaires_entity_type",
 *   label = @Translation("Type de questionnaires type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\questionnaires\TypeQuestionnairesEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\questionnaires\Form\TypeQuestionnairesEntityTypeForm",
 *       "edit" = "Drupal\questionnaires\Form\TypeQuestionnairesEntityTypeForm",
 *       "delete" = "Drupal\questionnaires\Form\TypeQuestionnairesEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\questionnaires\TypeQuestionnairesEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "type_questionnaires_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "type_questionnaires_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/questionnaires/settings/type_questionnaires_entity_type/{type_questionnaires_entity_type}",
 *     "add-form" = "/admin/structure/questionnaires/settings/type_questionnaires_entity_type/add",
 *     "edit-form" = "/admin/structure/questionnaires/settings/type_questionnaires_entity_type/{type_questionnaires_entity_type}/edit",
 *     "delete-form" = "/admin/structure/questionnaires/settings/type_questionnaires_entity_type/{type_questionnaires_entity_type}/delete",
 *     "collection" = "/admin/structure/questionnaires/settings/type_questionnaires_entity_type"
 *   }
 * )
 */
class TypeQuestionnairesEntityType extends ConfigEntityBundleBase implements TypeQuestionnairesEntityTypeInterface {

  /**
   * The Type de questionnaires type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Type de questionnaires type label.
   *
   * @var string
   */
  protected $label;

}

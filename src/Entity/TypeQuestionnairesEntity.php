<?php

namespace Drupal\questionnaires\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Type de questionnaires entity.
 *
 * @ingroup questionnaires
 *
 * @ContentEntityType(
 *   id = "type_questionnaires_entity",
 *   label = @Translation("Type de questionnaires"),
 *   bundle_label = @Translation("Type de questionnaires type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\questionnaires\TypeQuestionnairesEntityListBuilder",
 *     "views_data" = "Drupal\questionnaires\Entity\TypeQuestionnairesEntityViewsData",
 *     "translation" = "Drupal\questionnaires\TypeQuestionnairesEntityTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\questionnaires\Form\TypeQuestionnairesEntityForm",
 *       "add" = "Drupal\questionnaires\Form\TypeQuestionnairesEntityForm",
 *       "edit" = "Drupal\questionnaires\Form\TypeQuestionnairesEntityForm",
 *       "delete" = "Drupal\questionnaires\Form\TypeQuestionnairesEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\questionnaires\TypeQuestionnairesEntityHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\questionnaires\TypeQuestionnairesEntityAccessControlHandler",
 *   },
 *   base_table = "type_questionnaires_entity",
 *   data_table = "type_questionnaires_entity_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer type de questionnaires entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/questionnaires/type_questionnaires_entity/{type_questionnaires_entity}",
 *     "add-page" = "/admin/structure/questionnaires/type_questionnaires_entity/add",
 *     "add-form" = "/admin/structure/questionnaires/type_questionnaires_entity/add/{type_questionnaires_entity_type}",
 *     "edit-form" = "/admin/structure/questionnaires/type_questionnaires_entity/{type_questionnaires_entity}/edit",
 *     "delete-form" = "/admin/structure/questionnaires/type_questionnaires_entity/{type_questionnaires_entity}/delete",
 *     "collection" = "/admin/structure/questionnaires/type_questionnaires_entity",
 *   },
 *   bundle_entity_type = "type_questionnaires_entity_type",
 *   field_ui_base_route = "entity.type_questionnaires_entity_type.edit_form"
 * )
 */
class TypeQuestionnairesEntity extends ContentEntityBase implements TypeQuestionnairesEntityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Type de questionnaires entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Type de questionnaires entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Type de questionnaires is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}

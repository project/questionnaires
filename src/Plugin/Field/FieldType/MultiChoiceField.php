<?php

namespace Drupal\questionnaires\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'multi_choice_field' field type.
 *
 * @FieldType(
 *   id = "multi_choice_field",
 *   label = @Translation("Multi choice field"),
 *   description = @Translation("Multi Choice Field Type"),
 *   default_widget = "multi_choice_widget",
 *   default_formatter = "multi_choice_formatter"
 * )
 */
class MultiChoiceField extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Text'))
      ->setRequired(TRUE);
    $properties['format'] = DataDefinition::create('filter_format')
      ->setLabel(t('Text format'));
    $properties['correct_answer'] = DataDefinition::create('boolean')
      ->setLabel(t('Correct answer'));
    $properties['processed'] = DataDefinition::create('string')
      ->setLabel(t('Processed text'))
      ->setDescription(t('The text with the text format applied.'))
      ->setComputed(TRUE)
      ->setClass('\\Drupal\\text\\TextProcessed')
      ->setSetting('text source', 'value');
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'value' => array(
          'type' => 'text',
          'size' => 'big',
        ),
        'format' => array(
          'type' => 'varchar_ascii',
          'length' => 255,
        ),
        'correct_answer' => array(
          'type' => 'int',
          'length' => 'tiny',
        ),
      ),
      'indexes' => array(
        'format' => array(
          'format',
        ),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public function onChange($property_name, $notify = TRUE) {

    // Unset processed properties that are affected by the change.
    foreach ($this->definition->getPropertyDefinitions() as $property => $definition) {
      if ($definition->getClass() == '\\Drupal\\text\\TextProcessed') {
        if ($property_name == 'format' || $definition->getSetting('text source') == $property_name) {
          $this->writePropertyValue($property, NULL);
        }
      }
    }
    parent::onChange($property_name, $notify);
  }
}

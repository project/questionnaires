<?php

namespace Drupal\questionnaires\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'multi_choice_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "multi_choice_formatter",
 *   label = @Translation("Multi choice formatter"),
 *   field_types = {
 *     "multi_choice_field"
 *   }
 * )
 */
class MultiChoiceFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = array(
        '#type' => 'processed_text',
        '#text' => $item->value,
        '#format' => $item->format,
        '#langcode' => $item->getLangcode(),
      );
      $format = $this->getSetting('format');
      $elements[$delta]['correct_answer'] = array(
        '#markup' => $item->correct_answer ? $this->t('True') : $this->t('False'),
      );
    }

    return $elements;
  }
}

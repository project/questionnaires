<?php

namespace Drupal\questionnaires\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Type de questionnaires entities.
 *
 * @ingroup questionnaires
 */
class TypeQuestionnairesEntityDeleteForm extends ContentEntityDeleteForm {


}

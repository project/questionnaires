<?php

/**
 * @file
 * Contains type_questionnaires_entity.page.inc.
 *
 * Page callback for Type de questionnaires entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Type de questionnaires templates.
 *
 * Default template: type_questionnaires_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_type_questionnaires_entity(array &$variables) {
  // Fetch TypeQuestionnairesEntity Entity Object.
  $type_questionnaires_entity = $variables['elements']['#type_questionnaires_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
